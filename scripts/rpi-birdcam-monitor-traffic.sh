#!/bin/sh

RRDDIR=/var/lib/rrd

# create rrd database if not exists
if [ ! -f "$RRDDIR/mobile-traffic.rrd" ]; then
  rrdtool create "$RRDDIR/mobile-traffic.rrd" --step 900 \
    DS:txbytes:DERIVE:3600:0:U \
    DS:rxbytes:DERIVE:3600:0:U \
    RRA:AVERAGE:0.5:1:672: \
    RRA:AVERAGE:0.5:96:28: \
    RRA:AVERAGE:0.5:2688:13
fi

# get traffic from network interface:
RXBYTES=`cat "/sys/class/net/ppp0/statistics/rx_bytes" || /bin/true`
TXBYTES=`cat "/sys/class/net/ppp0/statistics/tx_bytes" || /bin/true`

if [ "x$TXBYTES" = "x" ]; then
  TXBYTES="U"
fi

if [ "xRXBYTES" = "x" ]; then
  RXBYTES="U"
fi

rrdtool update "$RRDDIR/mobile-traffic.rrd" \
  "N:${TXBYTES}:${RXBYTES}"

rrdtool graph /var/www/html/stat/traffic.png \
  --full-size-mode -w 720 -h 480 -a PNG \
  --start -4w  \
  "DEF:txrate=${RRDDIR}/mobile-traffic.rrd:txbytes:AVERAGE" \
  "DEF:rxrate=${RRDDIR}/mobile-traffic.rrd:rxbytes:AVERAGE" \
  CDEF:txsum=txrate,STEPWIDTH,*,PREV,ADDNAN \
  CDEF:rxsum=rxrate,STEPWIDTH,*,PREV,ADDNAN \
  LINE1:txsum#FF0000:"Bytes gesendet" \
  LINE2:rxsum#0000FF:"Bytes empfangen"

