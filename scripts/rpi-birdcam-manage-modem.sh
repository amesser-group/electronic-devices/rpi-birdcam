#!/bin/sh
# encoding: utf-8
#
# Raspberry PI Birdcam u-blox LTE modem management script
#
# Copyright (C) 2019 Peggy Messer
# Copyright (C) 2019 Andreas Messer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# for debugging
#set -x

MODEMDEVICE=/dev/ttyAMA0
GPIO=/usr/bin/gpio

CHAT=/usr/sbin/chat
STTY=/bin/stty

START_BAUDRATE=115200
# Seems 3MBaud does not work reliable
# dont forget to adapt /etc/ppp/peers/mobile-provider if changing this */
OP_BAUDRATE=921600
#OP_BAUDRATE=3000000

# Exit this script if pppd is already running
if pidof /usr/sbin/pppd; then
  exit
fi

# setup gpios for uart flow control
gpio -1 mode 11 alt3
gpio -1 mode 36 alt3

# setup reset and power gpios

gpio -1 mode 29 out
gpio -1 mode 31 out

# reset modem
gpio -1 write 31 1 && sleep 0.1 && gpio -1 write 31 0 && sleep 4

# wake modem
gpio -1 write 29 1 && sleep 0.1 && gpio -1 write 29 0 && sleep 1

# configure uart for initial baud rate
$STTY -F ${MODEMDEVICE} crtscts ispeed ${START_BAUDRATE} ospeed ${START_BAUDRATE}

# wait for modem to be ready and ask it to change to operational baud rate
$CHAT -s -t 1 "" AT OK-AT-OK "AT+IPR=${OP_BAUDRATE}" OK '\c' >${MODEMDEVICE} <${MODEMDEVICE}

# configure uart for operation baud rate
$STTY -F ${MODEMDEVICE} crtscts ispeed ${OP_BAUDRATE} ospeed ${OP_BAUDRATE} && sleep 0.5

# check if modem is responding after baud change
$CHAT -s -t 1 "" AT OK-AT-OK "AT+CGMI" OK "AT+CGMR" "OK" '\c' >${MODEMDEVICE} <${MODEMDEVICE}

# disable network led
$CHAT -s -t 1 "" AT OK "AT+UGPIOC=16,255" OK '\c' >${MODEMDEVICE} <${MODEMDEVICE}

# enable lte only mode
$CHAT -s -t 1 "" AT OK "AT+URAT=3" OK '\c' >${MODEMDEVICE} <${MODEMDEVICE}

# give sim some time to be detected
sleep 5
# enter sim pin
$CHAT -s -t 1 "" 'AT+CPIN?' OK '@/etc/chatscripts/mobile-pin' OK '\c' >${MODEMDEVICE} <${MODEMDEVICE}

pon mobile-provider
