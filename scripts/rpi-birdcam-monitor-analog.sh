#!/bin/sh

RRDDIR=/var/lib/rrd


# create rrd database if not exists
if [ ! -f "$RRDDIR/rpi-hardware.rrd" ]; then
  rrdtool create "$RRDDIR/rpi-hardware.rrd" --step 300 \
    DS:count0:GAUGE:900:0:4095 \
    DS:count1:GAUGE:900:0:4095 \
    DS:count2:GAUGE:900:0:4095 \
    DS:vbat:GAUGE:900:8:16 \
    DS:rphoto:GAUGE:900:0:100000000 \
    DS:rtemp:GAUGE:900:0:100000 \
    RRA:AVERAGE:0.5:1:2016: \
    RRA:AVERAGE:0.5:12:4708: \
    RRA:AVERAGE:0.5:288:365
fi

# Database will be filled with data by daemon
#rrdtool update "$RRDDIR/mobile-traffic.rrd" \
#  "N:${TXBYTES}:${RXBYTES}"

# generate graphics
for i in -1d -3d -2w -52w; do
  rrdtool graph /var/www/html/stat/vbat${i}.png \
    --full-size-mode -w 720 -h 480 -a PNG \
    -u 16.0 -l 10.0 -r \
    --left-axis-format "%.1lf" \
    --start ${i}  \
    "DEF:v=${RRDDIR}/rpi-hardware.rrd:vbat:AVERAGE" \
    LINE1:v#FF0000:"Batteriespannung"

  rrdtool graph /var/www/html/stat/rtemp${i}.png \
    --full-size-mode -w 720 -h 480 -a PNG \
    --start ${i}  \
    "DEF:r=${RRDDIR}/rpi-hardware.rrd:rtemp:AVERAGE" \
    LINE1:r#FF0000:"NTC Widerstand"

  rrdtool graph /var/www/html/stat/rphoto${i}.png \
    --full-size-mode -w 720 -h 480 -a PNG \
    --start ${i}  \
    -o \
    "DEF:r=${RRDDIR}/rpi-hardware.rrd:rphoto:AVERAGE" \
    LINE1:r#FF0000:"Fotowiderstand"

  rrdtool graph /var/www/html/stat/temp${i}.png \
    --full-size-mode -w 720 -h 480 -a PNG \
    --start ${i}  \
    "DEF:r=${RRDDIR}/rpi-hardware.rrd:rtemp:AVERAGE" \
    "CDEF:t=1,r,10000,/,LOG,4300,/,1,298.15,/,+,/,273.15,-" \
    LINE1:t#FF0000:"Temperatur"

  rrdtool graph /var/www/html/stat/lux${i}.png \
    --full-size-mode -w 720 -h 480 -a PNG \
    --start ${i}  \
    -o \
    "DEF:r=${RRDDIR}/rpi-hardware.rrd:rphoto:AVERAGE" \
    "CDEF:l=r,-1.4,POW,1e7,*" \
    LINE1:l#FF0000:"Lux (unkalibriert)"
done
