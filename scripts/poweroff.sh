#!/bin/sh
# encoding: utf-8
#
# Raspberry PI Birdcam basboard power supply script
#
# Copyright (C) 2019 Andreas Messer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script will shutdown the powersupply of the RPI BirdCam baseboard when
# systemd powers shutds down the system with target "poweroff"

if [ "x$1" = "xpoweroff" ]; then
  # Setting board pin 15 to low will shutdown the
  # baseboard's 5V DC/DC regulator
  
  gpio -1 mode 15 out
  gpio -1 write 15 0
fi

