#!/bin/sh

HTMLDIR=/var/www/html

( cd "${HTMLDIR}/snapshot" && ls -t | tail -n +10 | xargs /bin/rm )
( find "${HTMLDIR}/video" -daystart -ctime +84 -type f -delete )
