#!/usr/bin/env python
# encoding: utf-8
#
# Raspberry PI Birdcam hardware management daemon
# Copyright (C) 2019 Peggy Messer
# Copyright (C) 2019 Andreas Messer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import RPi.GPIO as gpio
import spidev
import subprocess
import time
import struct
from daemonize import Daemonize

import argparse
import collections
import logging
import serial
import os.path
import datetime



class RPIBirdCamHwManager(object):
    '''Hardware manager for rpi-birdcam baseboard'''

    rrd_database   = "/var/lib/rrd/rpi-hardware.rrd"

    # Define a type to hold the the analog data derived from RPI BirdCam baseboards's ADC
    AnalogState = collections.namedtuple('AnalogState', ['datetime', 'counts', 'ubat', 'rphoto', 'rtemp'])

    def __init__(self, log):
        '''Constructor'''
        super(RPIBirdCamHwManager, self).__init__()

        # store log object
        self.log = log

        self.analog_state    = self.AnalogState(datetime.datetime.now(), (None,None,None), None, None, None)
        self.last_rrd_update = None

    def init_hardware(self):
        '''Init hardware for using in the daemon'''
        gpio.setmode(gpio.BOARD)

        # There is a memory lieak in spidev implementation
        # unreferenced spidev objects will not be garbe collected du to invalid reference counting
        # therefore allocate one instance permanently
        self.spi = spidev.SpiDev()

    def read_adc_value(self,channel):
        '''Read value from adc on baseboard'''

        spi = self.spi
        spi.open(0,0)
        try:
            spi.max_speed_hz = 100000
            result = spi.xfer([ 0x06, channel << 6, 0x00])
            counts = (result[1] & 0x0f) * 256 + result[2]
        finally:
            spi.close()

        return counts

    def update_analog_values(self):
        ''' Get counts from adc and compute related analog values '''

        # Get current time
        timestamp = datetime.datetime.now()
        # read values from adc
        counts = tuple(self.read_adc_value(x) for x in range(3))

        # calculate analog values from adc result
        ubat = (2.5 * (22. + 2. + 4.7) * counts[0]) / (4095. * 4.7)

        if counts[1] > 0:
            rphoto = 100000. * (4095. / counts[1] - 1.)
        else:
            rphoto = None

        if counts[2] > 0:
            rtemp = 10000. * (4095. / counts[2] - 1.)
        else:
            rtemp = None

        # Update state propertiy with current values
        self.analog_state = self.AnalogState(timestamp, counts, ubat, rphoto, rtemp)


    def format_rrd_value(self,value):
        ''' Helper function to format a value into rrdtool's format '''
        if value is None:
            return 'U'
        else:
            return '{:f}'.format(value)

    def manage_analog_values(self):
        self.update_analog_values()

        rrd_update_interval = datetime.timedelta(seconds=60*5)
        current_time        = datetime.datetime.now()

        if os.path.exists(self.rrd_database):
            if (self.last_rrd_update is None) or \
               ((current_time - self.last_rrd_update) >= rrd_update_interval):

                analog_state = self.analog_state

                timestamp_str = '{:.0f}'.format(time.mktime(analog_state.datetime.timetuple()))

                count0_str = self.format_rrd_value(analog_state.counts[0])
                count1_str = self.format_rrd_value(analog_state.counts[1])
                count2_str = self.format_rrd_value(analog_state.counts[2])

                ubat_str    = self.format_rrd_value(analog_state.ubat)
                rphoto_str  = self.format_rrd_value(analog_state.rphoto)
                rtemp_str   = self.format_rrd_value(analog_state.rtemp)

                # call rrdtool to update database
                subprocess.call([
                    'rrdtool', 'update', self.rrd_database,
                    '{}:{}:{}:{}:{}:{}:{}'.format(timestamp_str,
                                                  count0_str, count1_str, count2_str,
                                                  ubat_str,rphoto_str,rtemp_str)
                ])

                self.last_rrd_update = analog_state.datetime

    def daemon_main(self):
        self.init_hardware()

        # Endlosschleife
        while(True):
            try:
                self.manage_analog_values();
            except Exception, e:
                self.log.error(u'Exception {!s} occured when handling analog values'.format(e))

            time.sleep(1)

if __name__ == '__main__':
    # Script execution starts from here

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='RPI BirdCam hardware management daemon')

    parser.add_argument('--pidfile', type=str, default='/var/run/rpi-birdcam-hwd.py',
                        help='pid file of daemon')

    parser.add_argument('--daemonize', const=True, default=False, action='store_const',
                        help='Go to background')

    args = parser.parse_args()

    # Setup logger for message logging
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    if args.daemonize:
        # Make all messages go to system log
        log_handler = logging.handlers.SysLogHandler(address = '/dev/log')
        log.addHandler( log_handler)
    else:
        log_handler = logging.StreamHandler()
        log.addHandler( log_handler)

    log.info('RPI BirdCam hardware management daemon starting')

    # Create instance oh hardware manager class
    mgr = RPIBirdCamHwManager(log)

    if args.daemonize:
        # do not close log file descriptor
        keep_fds = [log_handler.socket.fileno()]

        # start the daemon
        daemon = Daemonize(app='rpi-birdcam-hwd', pid = args.pidfile, action=mgr.daemon_main, keep_fds=keep_fds)
        daemon.start()
    else:
        # directly run main function
        mgr.daemon_main()

