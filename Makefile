ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

all:

clean:

install_network:
	install -d $(DESTDIR)/etc/network/interfaces.d/
	install -d $(DESTDIR)/etc/resolvconf/
	install -m 644 etc/network/interfaces.d/wlan-ap $(DESTDIR)/etc/network/interfaces.d/
	install -m 644 etc/resolvconf/interface-order $(DESTDIR)/etc/resolvconf/

install_mobile:
	install -d $(DESTDIR)/etc/chatscripts/
	install -d $(DESTDIR)/etc/network/interfaces.d/
	install -d $(DESTDIR)/etc/ppp/peers/
	install -d $(DESTDIR)$(PREFIX)/sbin/
	install -m 600 etc/chatscripts/mobile-provider             $(DESTDIR)/etc/chatscripts/
	install -m 600 etc/chatscripts/mobile-provider-disconnect  $(DESTDIR)/etc/chatscripts/
	install -m 644 etc/network/interfaces.d/mobile             $(DESTDIR)/etc/network/interfaces.d/
	install -m 600 etc/ppp/peers/mobile-provider               $(DESTDIR)/etc/ppp/peers/
	install -m 755 scripts/rpi-birdcam-manage-modem.sh         $(DESTDIR)$(PREFIX)/sbin/

install_motion:
	install -d $(DESTDIR)/etc/rpi-birdcam/
	install -m 644 etc/rpi-birdcam/motion.conf $(DESTDIR)/etc/rpi-birdcam/

install_lighttpd:
	install -d $(DESTDIR)/etc/lighttpd/conf-enabled/
	install -m 644 etc/lighttpd/conf-enabled/99-rpi_birdcam.conf $(DESTDIR)/etc/lighttpd/conf-enabled/

install_rrd:
	install -m 755 -d $(DESTDIR)/var/lib/rrd/

install: install_network install_mobile install_motion install_lighttpd install_rrd
	install -d $(DESTDIR)/lib/udev/rules.d/
	install -d $(DESTDIR)/etc/cron.d/
	install -m 644 etc/cron.d/rpi-birdcam $(DESTDIR)/etc/cron.d/
	install -d $(DESTDIR)$(PREFIX)/sbin/
	install -m 755 scripts/rpi-birdcam-hwd.py $(DESTDIR)$(PREFIX)/sbin/
	install -m 755 scripts/rpi-birdcam-monitor-traffic.sh $(DESTDIR)$(PREFIX)/sbin/
	install -m 755 scripts/rpi-birdcam-monitor-analog.sh $(DESTDIR)$(PREFIX)/sbin/
	install -m 755 scripts/rpi-birdcam-housekeeping.sh $(DESTDIR)$(PREFIX)/sbin/
	install -d $(DESTDIR)/lib/systemd/system-shutdown/
	install -m 755 scripts/poweroff.sh $(DESTDIR)/lib/systemd/system-shutdown/
	install -m 644 misc/99-rpi-birdcam.rules $(DESTDIR)/lib/udev/rules.d/

.PHONY: all clean install install_motion
